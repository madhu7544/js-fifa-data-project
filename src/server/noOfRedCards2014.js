function noOfRedCards2014(matches,players,year){
    let matchesInaYear = matches
        .filter((match)=>{
            if (match.Year == year){
                return match
            }
        })
        .reduce((acc,match)=>{
            acc.push(match.MatchID)
            return acc
        },[])     
    
    let nofRedCardsOfTeam= players.reduce((acc,player)=>{
        if (((player.Event).indexOf('R') !== -1) && (matchesInaYear.indexOf(player.MatchID) !== -1)){
            let teamName = player['Team Initials'];
                if (!acc[teamName]){
                    acc[teamName] =1;
                }else{
                    acc[teamName] +=1;
                } 
        }
        return acc
    },{})
    
    return nofRedCardsOfTeam 
}
    
module.exports = noOfRedCards2014

