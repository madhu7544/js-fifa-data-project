function topTenPlayers(players){
    const playerGoals = players.reduce((acc,player)=>{
        const events = player.Event.split('');
        const goals = events.filter(event => event.includes('G')).length;
       if(goals!=0){
            if (acc[player['Player Name']]){
                acc[player['Player Name']] += goals
            }else{
                acc[player['Player Name']] = goals
            }
       }
       return acc
    },{})

    let playerMatches = players.reduce((acc,player)=>{
        if(acc[player['Player Name']]){
            acc[player['Player Name']] += 1
        }else{
            acc[player['Player Name']] = 1
        }
        return acc
    },{})

   

    let probabilityOfPlayer = Object.keys(playerGoals).reduce((acc,ind)=>{
        acc[ind] = playerGoals[ind]/playerMatches[ind]
        return acc
    },{})

    let topPlayers = Object.entries(probabilityOfPlayer)
    let topPlayersProbability = topPlayers.sort((a,b)=> b[1]-a[1])
    let top10Players = Object.fromEntries(topPlayersProbability.slice(0,10))

    console.log(top10Players)
    return top10Players

     
}


module.exports = topTenPlayers;