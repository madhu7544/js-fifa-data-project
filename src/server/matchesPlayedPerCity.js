function getMatchesPlayedPerCity(matches){
  const matchesPerCity = matches
    .filter(match => match.City !== '')
    .reduce((acc, match) => {
      const city = match.City;
      if (!acc[city]){
        acc[city] = 1
      }else{
        acc[city] +=1
      }
      return acc;
  }, {});
  
return matchesPerCity;






//   const filterCities =matches.filter((match)=>{
//     if(match.City !==""){
//       return match
//     }
//   });
  
//   const cities = filterCities.map((match)=>match.City)
  
//   const matchesPerCity = cities.reduce((acc,city)=>{
//     if (!acc[city]){
//       acc[city] = 1
//     }
//     else{
//       acc[city] +=1
//     }
//     return acc
// },{})

//   return matchesPerCity;
}

module.exports = getMatchesPlayedPerCity








