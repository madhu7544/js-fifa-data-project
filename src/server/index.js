const csv= require('csvtojson')
const fs = require('fs');

const worldCupMatches ='../data/WorldCupMatches.csv';
const worldCupPlayers ='../data/WorldCupPlayers.csv';

const matchesPerCity = require('./matchesPlayedPerCity.js')
const wonMatchesPerTeam = require('./matchesWonPerTeam.js')
const noOfRedCards2014 =require('./noOfRedCards2014.js')
const topTenPlayers =require('./topTenPlayers.js')



csv()
.fromFile(worldCupMatches)
.then((matches)=>{
    //problem 1
    const result1 = JSON.stringify(matchesPerCity(matches));
    fs.writeFile('../public/output/matchesPlayedPerCity.json',result1,(error)=>{
        if(error){
          console.log(error);
        }else{
          console.log('The data of matchesPlayedPerCity was stored in JSON')
        }
      });

    //problem 2
      const result2 = JSON.stringify(wonMatchesPerTeam(matches));
      
      fs.writeFile('../public/output/matchesWonPerTeam.json',result2,(error)=>{
        if(error){
          console.log(error);
        }
        else{
          console.log('The data of MatchesWonPerTeam was stored in JSON')
        }
      });

      // problem 3
      csv()
      .fromFile(worldCupPlayers)
      .then((players)=>{
        const result3 = JSON.stringify(noOfRedCards2014(matches,players,2014));
        fs.writeFile('../public/output/noOfRedCards2014.json',result3,(error)=>{
          if(error){
            console.log(error);
          }else{
            console.log('The data of noOfRedCards2014 was stored in JSON')
          }
        });

        // problem 4
        const result4 = JSON.stringify(topTenPlayers(players));
        fs.writeFile('../public/output/topTenPlayers.json',result4,(error)=>{
          if(error){
            console.log(error);
          }else{
            console.log('The data of topTenPlayers was stored in JSON')
          }
        });
      });

  });
