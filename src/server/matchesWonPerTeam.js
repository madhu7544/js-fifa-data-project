function matchesWonPerTeam(matches) {
  let winningTeamsCount = matches.reduce((acc, match) => {
    let homeTeamGoals = match['Home Team Goals']
    let awayTeamGoals = match['Away Team Goals']
    let winnerTeam = (homeTeamGoals > awayTeamGoals) ? match['Home Team Name'] : (homeTeamGoals < awayTeamGoals) ? match['Away Team Name'] : null;
    if (winnerTeam != null) {
      if (!acc[winnerTeam]) {
        acc[winnerTeam] = 1
      }
      else {
        acc[winnerTeam] += 1
      }
    }

    return acc

  }, {})
  return winningTeamsCount

}

module.exports = matchesWonPerTeam;

